'use strict';

// const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');

const NODE_ENV = 'development';

module.exports = {
	context: __dirname + '/frontend',

	entry: {
		home: './home',
		about: './about'
		// common: ['./common', './welcome'] // Several files
	},

	output: {
		path: __dirname + '/public',
		filename: '[name].js',
		library: '[name]' // Global variables like entryes name's
	},

	watch: NODE_ENV == 'development',
	watchOptions: {
		aggregateTimeout: 100
	},

	devtool: NODE_ENV == 'development' ? 'cheap-inline-module-source-map' : false,

	plugins: [
		// Correct working
		new webpack.NoEmitOnErrorsPlugin(),
		// Передает переменную NODE_ENV в скрипт
		new webpack.DefinePlugin({
			NODE_ENV: JSON.stringify(NODE_ENV),
			LANG: JSON.stringify('ru')
		}),
		// Общая часть кода подключается в common
		new webpack.optimize.CommonsChunkPlugin({
			name: 'common',
		})
	],


	module: {
		// Babel-loader
	  loaders: [{
		  test: /\.js$/,  // Все файлы, заканчивающиеся на .js
		  exclude: /(node_modules|bower_components)/,
		  loader: 'babel-loader?optional[]=runtime',
		  query: {
		    presets: ['es2015']
		  }
		}]
	}

};

// if(NODE_ENV == 'production') {
Uglify JS code
	module.exports.plugins.push(
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: 	  false,
				drop_console: true,
				unsafe: 	 		true
			}
		})
	)
// }
